import React from 'react';
import moment from 'moment'

import Hidden from '@material-ui/core/Hidden';

import { useTheme } from '@material-ui/core/styles';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';

// Generate Sales Data

export default function Chart({data}) {
  
	const theme = useTheme();
  
	const formattedData = [];
  
	data.map((item) =>
		formattedData.push({time : Date.parse(item[1]), value : item[0]})
	);
	// propData.map((prop, key) => { return(
		// formattedData.push({time : prop[0], amount : prop[1]})
	// )});

  return (
    <React.Fragment>
		
	{/* <Title>Today</Title> */}
      <ResponsiveContainer>
        <LineChart
          data={formattedData}
          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 16,
          }}
        >
		{/* <XAxis dataKey="time" stroke={theme.palette.text.secondary} />    tickFormatter = {(unixTime) => moment(unixTime).format('HH:mm Do')}    domain = {['auto', 'auto']} */}
		<XAxis dataKey="time" stroke={theme.palette.text.secondary} type = 'number' tickFormatter = {(unixTime) => moment(unixTime).format('MMM Do YY HH:mm')} domain = {['auto', 'auto']}/>
			<YAxis stroke={theme.palette.text.secondary}>
			<Label angle={270} position="left" style={{ textAnchor: 'middle', fill: theme.palette.text.primary }} >
              Humidity (?)
			  </Label> 
          </YAxis> 
          <Line type="monotone" dataKey="value" stroke={theme.palette.primary.main} dot={false} />
        </LineChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}