import React from 'react';


// @material-ui core components
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
// @material-ui icons
import AssignmentIcon from '@material-ui/icons/Assignment';

// Routing
import routes from "views/routes.js";
import { NavLink } from "react-router-dom";

export const mainListItems = (
	<div>
	
	{routes.map((prop, key) => { return(
	<NavLink to={prop.path} key={key}  style={{ textDecoration: 'none', color : 'black' }}>
		<ListItem button>
			<ListItemIcon>
				<prop.icon />
			</ListItemIcon>
			<ListItemText primary={prop.text} />
		</ListItem>
	</NavLink>
	)})}
	</div>
);

export const secondaryListItems = (
  <div>
    <ListSubheader inset>Saved Sensors</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Vermi Humidity" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Vermi Temperature" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Vermicompost Tea Level" />
    </ListItem>
  </div>
);