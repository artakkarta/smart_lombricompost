import React from 'react';
import clsx from 'clsx';

// @material-ui core 
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

//App components
import Title from './Title';

//styles
import { makeStyles } from '@material-ui/core/styles';
import styles from "resources/jss/commonStyles.js";
const useStyles = makeStyles(styles);

// function preventDefault(event) {
  // event.preventDefault();
// }



export default function Deposits({title, value, type,lg}) {
  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  // ici faire toutes les manips avec le type etc
  return (
    <Grid item xs={12} md={4} lg={lg}>
		<Paper className={fixedHeightPaper}>
			<React.Fragment>
			  <Title>{title}</Title>
			  <Typography component="p" variant="h4">
			  {parseFloat(value).toFixed(2)}
			  </Typography>
		 
			  <div>
				<Typography color="textSecondary" className={classes.depositContext}>
					{type}
			  </Typography>
			  </div>
			</React.Fragment>
		</Paper>
    </Grid>
  );
}