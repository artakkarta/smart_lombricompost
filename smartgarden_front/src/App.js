import React from 'react';
import clsx from 'clsx';

// @material-ui core components
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';

//pour naviguer entre les pages horizontalement, mise en place d'un react-router
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import routes from "views/routes.js";

// App components
import { mainListItems, secondaryListItems } from './components/listViews';

// App styles
import { makeStyles } from '@material-ui/core/styles';
import styles from "resources/jss/commonStyles.js";
const useStyles = makeStyles(styles);

const hist = createBrowserHistory();



export default function Dashboard() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://monsiteweb">
        Luca Cattaneo
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

return (
    <div className={classes.root}>
      <CssBaseline />
	  
      {/* Top application bar */}
	  <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          {/* Button for opening the side menu */}
		  <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
		  {/* App title */}
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            -- Smart Garden --
          </Typography>
		  {/* Notification icon */}
          <IconButton color="inherit">
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>
	  
	  
<Router history={hist}>
	{/* Drawer Navigation menu */}
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
	  {/* Button for closing the side menu */}
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
		{/* List of menu items */}
        <List>{mainListItems}</List>
        <Divider />
        <List>{secondaryListItems}</List>
      </Drawer>
	  
	  {/* Main part with router to pages */}
      <main className={classes.content}>


			<Switch>
				{routes.map((prop, key) => {
					return (
						<Route
						path={prop.path}
						component={prop.component}
						key={key}
						/>
					);
				})}
				<Redirect from="/" to="/home" />
			</Switch>
		
		<Box pt={4}>
           <Copyright />
        </Box>
        
      </main>
	  
</Router>  
	  
    </div>
  );

}