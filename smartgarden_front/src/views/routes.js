// import React from 'react';

// @material-ui core components
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import BarChartIcon from '@material-ui/icons/BarChart';
// @material-ui icons
import LayersIcon from '@material-ui/icons/Layers';
// import AssignmentIcon from '@material-ui/icons/Assignment';

//App components
import HomeView from 'views/home.js';
import DataView from 'views/data.js';

const routesList = [
	{
		name: "Home",
		path: "/home",
		component: HomeView,
		text: "Home",
		icon: DashboardIcon
	},
	{
		name: "Data",
		path: "/data",
		component: DataView,
		text: "View Data",
		icon : BarChartIcon
	},
	{
		name: "Settings",
		path: "/settings",
		component: null,
		text: "Settings",
		icon : LayersIcon
	},
	{
		name: "Users",
		path: "/users",
		component: null,
		text: "Users",
		icon : PeopleIcon
	},
]

export default routesList;