import React, {useState, useEffect } from 'react';
// import clsx from 'clsx';
import moment from 'moment'

//import @material-ui core components
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';


//import app components
import DataWidget from 'components/DataWidget.js'

// styles
import { makeStyles } from '@material-ui/core/styles';
import styles from "resources/jss/commonStyles.js";
const useStyles = makeStyles(styles);



export default function Home() {
	
	var initial={'10':[],'20':[],'30':[]}
	const [data, setData] = useState(initial);
	
	useEffect(() => {
		fetch('/last_data').then(res => res.json()).then(data => {
		  setData(data);
		});
	}, []);

	const classes = useStyles();
	
	  // const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
	
	return (
		<div>
		<div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3} justify="center">
            {/* Widget grid */}
            <DataWidget title='Vermicompost Humidity' value={data[10][0]} type={moment(Date.parse(data[10][1])).format('MMM Do YY HH:mm')} lg={3}/>
			{/* <Grid item xs={12} md={4} lg={3} /> */}
			<DataWidget title='Vermicompost Temperature' value={data[20][0]} type={moment(Date.parse(data[20][1])).format('MMM Do YY HH:mm')} lg={3}/>
			<DataWidget title='Vermicompost Tea Level' value={data[30][0]} type={moment(Date.parse(data[30][1])).format('MMM Do YY HH:mm')} lg={3}/>
          </Grid>
        </Container>
		</div>
	);	
}