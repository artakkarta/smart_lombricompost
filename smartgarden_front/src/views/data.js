import React, {useState, useEffect } from 'react';
import clsx from 'clsx';

//import @material-ui core components
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import { makeStyles } from '@material-ui/core/styles';

//import app components
import Chart from 'components/Chart';
import DataTable from 'components/DataTable';
import styles from "resources/jss/commonStyles.js";
const useStyles = makeStyles(styles);



export default function Home() {

	const classes = useStyles();
		
	const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
	
	// props and props setter
	const [data, setData] = React.useState([]);
	const [typeData, setTypeData] = React.useState('');
	const [period, setPeriod] = React.useState('');
	
	// at the pag loading
	useEffect(() => {
		getData(20)
		setPeriod(10);
	}, []);
	
	//fetch data from Flask server
	// sensor -> sensor number 
	const getData = (sensor) => {
		setTypeData(sensor);
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ type: sensor })
		};
		fetch('/all_data',requestOptions).then(res => res.json()).then(data => {
		  setData(data);
		});
	}

	// handle Slectors changes
	const handleChangeSelectData = (event) => {
		setTypeData(event.target.value);
		getData(event.target.value);
	};
	const handleChangeSelectPeriod = (event) => {
		setPeriod(event.target.value);
	};
	
	return (
		<div>
		<div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
			{/* Dropdown */}
            <Grid item xs={6} md={8} lg={6}>
				<Paper className={classes.paper}>
					<FormControl variant="outlined" className={classes.formControl} width="50%">
						<InputLabel >Display data</InputLabel>
							<Select id="select_data" value={typeData} onChange={handleChangeSelectData} label="Display data">
								<MenuItem value=""><em>None</em></MenuItem>
								<MenuItem value={10}>Vermicompost Humidity </MenuItem>
								<MenuItem value={20}>Vermicompost Temperature</MenuItem>
								<MenuItem value={30}>Vermicompost Tea Level</MenuItem>
						</Select>
					</FormControl>
				</Paper>
            </Grid>
			 <Grid item xs={6} md={8} lg={6}>
				<Paper className={classes.paper}>
					<FormControl variant="outlined" className={classes.formControl}>
						<InputLabel >Time Period</InputLabel>
							<Select id="select_data" value={period} onChange={handleChangeSelectPeriod} label="Time Period">
								<MenuItem value=""><em>None</em></MenuItem>
								<MenuItem value={10}>Daily</MenuItem>
								<MenuItem value={20}>Weekly</MenuItem>
								<MenuItem value={30}>Montly</MenuItem>
								<MenuItem value={40}>Yearly</MenuItem>
						</Select>
					 </FormControl>
				</Paper>
            </Grid>
					{/* Chart */}
            <Grid item xs={12} md={8} lg={12}>
				<Paper className={fixedHeightPaper}>
					
					<Chart data={data} type={typeData}/>
				</Paper>
            </Grid>
            {/* Data Table */}
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <DataTable data={data} type={typeData}/>
              </Paper>
            </Grid>
          </Grid>
        </Container>
		</div>
	);	
}