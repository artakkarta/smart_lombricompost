#!/usr/bin/env python3

import sqlite3

class Database:
    #"""sqlite3 database class that holds testers jobs"""

    def __init__(self,name=None): 
        self.conn = None
        self.cursor = None
        if name:
            self.open(name)
        
        
    def open(self,name):
    #"""open sqlite3 connection"""
        try:
            self.conn = sqlite3.connect(name)
            self.cursor = self.conn.cursor()
        except sqlite3.Error as e:
            print("Error connecting to database!")

    def close(self):
    #    """close sqlite3 connection"""
        if self.conn:
            self.conn.commit()
            self.cursor.close()
            self.conn.close()
         
    def __exit__(self,exc_type,exc_value,traceback):
        self.close()

    def query(self, sql):
    #    """execute a row of data to current cursor"""
        return self.cursor.execute(sql).fetchall()

    def commit(self):
    #    """commit changes to database"""
        self.connection.commit()
        
    def get(self,table,columns,limit=None):
        query = "SELECT {0} from {1};".format(columns,table)
        self.cursor.execute(query)
        # fetch data
        rows = self.cursor.fetchall()
        rows = rows[len(rows)-limit if limit else 0:]
        return rows
        
    def get_sensor_measures(self,sensor,limit=None):
        query = "SELECT value,time FROM measures WHERE sensor={0};".format(sensor)
        self.cursor.execute(query)
        # fetch data
        rows = self.cursor.fetchall()
        rows = rows[len(rows)-limit if limit else 0:]
        return rows
        
    def getLast_sensor_measures(self,sensor,limit=None):
        return self.get_sensor_measures(sensor,limit=1)[0]
        
    def getLast(self,table,columns):
        return self.get(table,columns,limit=1)[0]
        
    def write(self,table,columns,data):       
        query = "INSERT INTO {0} ({1}) VALUES ({2});".format(table,columns,data)
        self.cursor.execute(query)