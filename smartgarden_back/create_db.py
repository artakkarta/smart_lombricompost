#!/usr/bin/env python3

# Script for the creation of an empty database used on the smargarden software
# 1 argument necessary
# dbname : name of the sqlite3 database to be created

import sqlite3
import sys

#check arguments
# print("len : {0}".format(len(sys.argv)))
# print("arg : {0}".format(sys.argv[0]))
if len(sys.argv) != 2:
    print("---------------- ERROR (create_db.py) ----------------")
    print("Only one argument is required for create_db.py dbname")
    print("\t- dbname : name of the sqlite3 database to be created")
    exit()  

# name with location of database to create
dbname = sys.argv[1]

conn = sqlite3.connect(dbname)
cursor = conn.cursor()

# table MEASURES
sql="CREATE TABLE IF NOT EXISTS 'measures' ('id' INTEGER NOT NULL PRIMARY KEY, 'sensor' INTEGER, 'value' DECIMAL, 'time' DATETIME)"
cursor.execute(sql)
conn.commit()

# table SENSORS
sql="CREATE TABLE IF NOT EXISTS 'sensors' ('id' INTEGER NOT NULL PRIMARY KEY, 'name' TEXT, 'connection' INTEGER, 'sensor_use' DATETIME)"
cursor.execute(sql)
conn.commit()

cursor.close()
conn.close()