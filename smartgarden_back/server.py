from flask import Flask, jsonify, request
from extractData import Database

app = Flask(__name__)

# databaseName = "sensors_data.db"
databaseName = "smartgarden_db.db"

@app.route('/test')
def get_current_time():
    return {'test': '0'}

@app.route('/last_data', methods = ['GET'])
def get_last_data():
    record = []
    db = Database(databaseName)
    record.append(db.getLast_sensor_measures(2))
    record.append(db.getLast_sensor_measures(1))
    record.append(db.getLast_sensor_measures(3))
    db.close()
    # return jsonify(record)
    return {
        '10':record[0],
        '20':record[1],
        '30':record[2],
    }
    
@app.route('/all_data', methods = ['POST'])
def get_unique_data():
    request_data = request.get_json()
    sensor = ''
    if request_data['type'] == 10:
        sensor =2      # moisture
    elif request_data['type'] == 20:
        sensor =1       # temperature
    elif request_data['type'] == 30:
        sensor=3        # distance
    else:
        exit()
    
    db = Database(databaseName)
    record = db.get_sensor_measures(sensor)
    #record = db.query("SELECT * FROM measures_table")
    db.close()
    return jsonify(record)